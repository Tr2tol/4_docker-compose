# Используем базовый образ Maven с Java 17
#FROM maven:3.8.4-openjdk-17

# Копирование исходного кода проекта в контейнер
#COPY . /app
#WORKDIR /app

# Сборка проекта Maven
#RUN mvn clean install

# Запуск приложения
#CMD ["java", "-jar", "/app/target/hallo-0.0.1-SNAPSHOT.jar"]

# Stage 1: Сборка проекта Maven
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn package -DskipTests \
    && rm -rf /root/.m2

# Stage 2: Запуск приложения
FROM openjdk:17-jdk-alpine
WORKDIR /app
COPY --from=build /app/target/*.jar /app/app.jar
CMD ["java", "-jar", "/app/app.jar"]
RUN rm -rf /root/.m2 \
    && rm -rf /tmp/*
